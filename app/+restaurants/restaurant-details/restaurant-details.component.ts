import { Component, Input, forwardRef, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Events, Content,ToastController } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
// import { FileService } from '../../shared/services/file-service.service.ts';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { InAppBrowser } from 'ionic-native';
import {
  Favorites,
  AjmanMap,
  Restaurant,
  ImageSlider,
  TopBar,
  TTSService,
  TripAdvisor,
  ShareButton
} from '../../shared';
declare var cordova;

@Component({
  templateUrl: 'build/+restaurants/restaurant-details/restaurant-details.component.html',
  providers:[Favorites, TripAdvisor],
  directives: [forwardRef(() => AjmanMap), forwardRef(() => TopBar), forwardRef(() => TTSService), forwardRef(() => ShareButton)],
  styles: [`
  `]
})
export class RestaurantDetails {
  item: Restaurant;
  didLoad:boolean;
   exitPage: boolean = false;
  tripAdvisorContent:string = null;
  enterPage: boolean = true;
  scrollTweak: boolean = false;
  imgHeight: any = window['innerHeight'];
  scrollBar: boolean = false;
    detailsBG: boolean = false;
  @ViewChild('detailScroll') detailScroll: Content;
  constructor(public translateService: TranslateService, public tripAdvisor: TripAdvisor,public toastCtrl: ToastController,public events: Events, private modalController : ModalController, public loadingCtrl: LoadingController, public navCtrl: NavController, params: NavParams, public favorites: Favorites) {
    var that = this;
    this.item = params.get("item");
    // this.fileService.getFile(this.item.featuredImage, "attraction")

    console.log(this.item);
    //this.presentLoading();
    events.subscribe("page:exit", function() {
       that.exitPage = true;
    });
  }
      ionViewDidEnter() {
      this.detailsBG = true
    }
  ngAfterViewInit() {
    var that = this;
    this.detailScroll.addScrollListener((event) => {
         var topPos = event.target.querySelector(".details-top").scrollHeight+50;
         this.enterPage = false;
         // if(event.target.scrollTop>topPos) {
         //   console.log("scrollTweak", true);
         //   this.scrollTweak = true;
         // } else {
         //   console.log("scrollTweak", false);
         //   this.scrollTweak = false;
         // }
         if (event.target.scrollTop > 10) {
            if(!this.scrollBar) {
              this.scrollBar = true;
            }
            
          } else {
            if(this.scrollBar) {
              this.scrollBar = false;  
            }
            
          }
    });
     if(this.item.tripAdvisorId)
      {
        this.tripAdvisor.getTripAdvisorDetailsForOneItem(this.item.tripAdvisorId).subscribe(res => {
          that.tripAdvisorContent = res;
        });
      }
  }
  presentLoading() {
    let loading = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000,
      dismissOnPageChange: false
    });
    loading.present();
    loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
  }
  
  getItems() {
    return [this.item];
  }

  openURL(day) {
    cordova.InAppBrowser.open('https://www.google.com/maps/place/'+day.latitude+','+ day.longitude+'/@'+day.location+'z', '_system')
  }
  openHotelURL(item){
    cordova.InAppBrowser.open(item.website, '_system');
  }

  toggleFavorite() {
    var str = "";

    if (this.favorites.itemIsFavorite(this.item.id)) {
      str = "Item removed from favorites!";
      this.item.favorite = false;
      this.favorites.removeFromFavorites(this.item.id);
    }
    else {
      str = "Added to favourites!";
      this.item.favorite = true;
      this.favorites.addToFavorites(this.item.id);
    }

    this.translateService.getTranslation(localStorage.getItem("currentLang")).subscribe(res => {
      if (res[str])
        str = res[str];
      var toast = this.toastCtrl.create({
        message: str,
        duration: 3000,
        position: 'bottom'
      })
      toast.present();
    });
    this.events.publish("favourites:change", true);

  }
  showImageSlider(){
    let modal = this.modalController.create(ImageSlider,{images:this.item.images});
    console.log(modal)
    modal.present();
  }

}
