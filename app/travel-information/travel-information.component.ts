import { Component, OnInit, forwardRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import {
	ContentService,
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar,
  	CardList,
  	DbService,
    HttpClient,
    AuthorizationService,
    ImageCache
} from '../shared'


@Component({
	templateUrl: 'build/travel-information/travel-information.component.html',
	providers: [
    ContentService,
    DbService,
    HttpClient,
    AuthorizationService,
    ImageCache,
  ],
  directives: [TopBar, forwardRef(() => CardList)]
})
export class TravelInformation implements OnInit {

	items: any = null;
	loading: any;
	didLoad: any;
	scrollBar: boolean = false;
	constructor(public loadingCtrl: LoadingController, private dbService: DbService) {
		this.loading = this.loadingCtrl.create({
	      content: "Please wait...",      
	      duration: 2000,
	      dismissOnPageChange: false
	    });
	    this.loading.onDidDismiss(()=>{
	      this.didLoad = true;
	    });
	    //this.presentLoading();
		}

	ngOnInit() {
		var that = this;

		// this.items = [1351, 1427, 1428, 1429, 1430, 2146, 2823, 2824, 2826, 2825, 2827, 2831]
		this.dbService.getFiltered({id: {$in : [1143, 3039]}}, [], ["id"], 0, 30).subscribe(res => {
	    	var items = res.items[0]['childrenPages'];
	    	that.items = items;
	    	console.log("trabelItems", res)
	
	    	// observer.next(items)
	    })
	}
	presentLoading() {
      this.loading.present();
    }
    dismissLoading() {
      //this.loading.dismiss();
    }
    scrolled(value) {
    this.scrollBar = value;
  }
}