import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ViewController, NavParams, Content } from 'ionic-angular';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { Http, Headers } from '@angular/http';
declare var twitterFetcher:any;
declare var Instafeed:any;
import * as moment from 'moment';

import { 
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar
} from '../shared'

declare var xml2json;
declare var cordova;


@Component({
	templateUrl: 'build/social-media/social-media.component.html',
	 directives: [TopBar],
	 providers: [TranslateService, TranslateLoader, LanguageService, 
    { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe]
})
export class SocialMedia implements OnInit {
  @ViewChild('detailScroll') detailScroll: Content;
  scrollBar: boolean = false;
	items: any = [];
	isLoaded: boolean = false;
	fbAccessToken: string = "EAAXyuoqRLZC4BAMtj0ZBshcIgTNGLUZAwItiFtt7fa5p6dKaz0mLEC7kW6XPL5gzXOlMZCjci74FwG7eqlQHhF7xglwFXTgLdMLT8xdZAR4EMReWRbIY4ZA5ThOFMVv2iJWmrLAlowZB2Y38Jy3EWQkcG8CdjcbEZBZCBxtrCeZBvqQwZDZD";
	fbContent: any;
	socialAccounts : any = {
		facebook: "@VisitAjman",
		instagram: "@ajman",
		twitter: "@VisitAjman",
		youtube: "@ajmantdd - Click here to watch"
	}
	constructor(public viewCtrl:ViewController,public params: NavParams, public http: Http) {}

	ngOnInit() {
		
		
	}
	private formatDate(date:string){
      return moment(date).format('DD MMM');
    }
	ngAfterViewInit() {
        this.detailScroll.addScrollListener((event) => {
             var topPos = event.target.querySelector(".social-top").scrollHeight+50;
             
             if(event.target.scrollTop>10) {
               this.scrollBar = true;
             } else {
               this.scrollBar = false;
             }
        });
    }

    makeArray(obj, type) {
    	var objArr = this.items;
    	
    	if(type=="twitter") {
    		for(var i in obj) {
    			var item = obj[i];
    			var objItem = <any>{};
    			objItem.message = item.tweet;
    			objItem.datetime = item.time;
    			objItem.date = this.formatDate(item.time)
    			objItem.image = item.image;
    			objItem.url = item.permalinkURL;
    			objItem.source = item.image;
    			objItem.type = "tweet";
    			objItem.provider = "twitter";
    			objItem.account = this.socialAccounts['twitter'],
    			objArr.push(objItem);
    		}
    	}

    	if(type=="facebook") {
    		for(var i in obj) {
    			var item = obj[i];
    			var objItem = <any>{};
    			objItem.message = item.message;
    			objItem.datetime = item.created_time;
    			objItem.date = this.formatDate(item.created_time)
    			objItem.image = item.full_picture;
    			objItem.url = item.permalink_url;
    			objItem.source = item.source;
    			objItem.type = item.type;
    			objItem.provider = "facebook";
    			objItem.account = this.socialAccounts['facebook'],
    			objArr.push(objItem);
    		}
    	}


    	if(type=="youtube") {
    		var countVid = 0;
    		for(var i in obj) {
    			if(countVid>3) { break; }
    			var item = obj[i];
    			var objItem = <any>{};
    			objItem.message = item.title;
    			objItem.datetime = item.published;
    			objItem.date = this.formatDate(item.published)
    			objItem.image = item['media:group']['media:thumbnail']['@url'];
    			objItem.url = item.link["@href"];
    			objItem.source = item['media:group']['media:content']['@url'];
    			objItem.type = "image";
    			objItem.provider = "youtube";
    			objItem.account = this.socialAccounts['youtube'];
    			countVid++;
    			objArr.push(objItem);
    		}
    	}

    	if(type=="instagram") {
    		for(var i in obj) {
    			var item = obj[i];
    			var objItem = <any>{};
    			var InstaDate = new Date(item.created_time*1000);
    			objItem.message = item.caption.text;
    			objItem.datetime = moment(InstaDate).format();
    			objItem.date = this.formatDate(objItem.datetime)
    			objItem.image = item.images.standard_resolution.url;
    			objItem.url = item.link;
    			objItem.source = null;
    			objItem.type = item.type;
    			objItem.provider = "instagram";
    			objItem.account = this.socialAccounts['instagram'],
    			console.log(objItem)
    			objArr.push(objItem);
    		}
    	}
    	
    	this.items = this.orderByDate(objArr, "datetime");

    }
    orderByDate(arr, dateProp) {
	  return arr.slice().sort(function (b, a) {
	    return a[dateProp] < b[dateProp] ? -1 : 1;
	  });
	}

    getFacebook() {
    	var that = this;
    	this.http.get('https://graph.facebook.com/v2.8/554173854600552/posts?fields=message,full_picture,description,permalink_url,source,type,created_time&limit=3&access_token='+this.fbAccessToken)
	    	.map(res => res.json())
	    	.subscribe(res => {
	    		that.makeArray(res.data, "facebook")
	    	}
    	)
    }

    getYoutube() {
    	var that = this;
    	var headers = new Headers();
    	headers.append('Accept', 'application/xml');
    	this.http.get("https://www.youtube.com/feeds/videos.xml?channel_id=UCNfV4mJe78PshF207CcMdUA&callback=?", { headers: headers}).
    	map(res => JSON.parse(xml2json(res.text(),'  '))).subscribe(res => {
    		that.makeArray(res.feed.entry, "youtube")
    	})
    }

	ionViewDidEnter() {
		var that = this;
		this.isLoaded = true;
		var config2 = {
		  "profile": {"screenName": 'visitajman'},
		  "domId": 'twitterDiv',
		  "maxTweets": 3,
		  'showInteractionLinks': false,
		  "enableLinks": true,
		  "showUser": false,
		  "showTime": true,
		  "lang": 'en',
		  dataOnly: true,
		  customCallback: function(tweets) {
		  	that.makeArray(tweets, "twitter")
		  }
		};
		twitterFetcher.fetch(config2);
		var feed = new Instafeed({
	        get: 'user',
	        userId: '843637',
	        clientId: 'b5b4d23a603b41928745737cd1b15b37',
	        accessToken: '843637.bde88ea.f34b26a310f44485bd6cc0720ac522d2',
	        limit: 3,
	        resolution: 'standard_resolution',
        	template: '<img src="{{image}}" />',
        	mock: true,
        	success: function(data) {
        		that.makeArray(data.data, "instagram")
        	}
	    });
	    feed.run();

	    this.getFacebook()
	    this.getYoutube()

	}

	openLink(url) {
		cordova.InAppBrowser.open(url, '_system')
	}

	
}