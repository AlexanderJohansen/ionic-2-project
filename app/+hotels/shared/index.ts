export { HotelRating } from './hotel-rating/hotel-rating.component';
export { Hotel } from '../shared/hotel.model';
export { HotelDetails } from '../hotel-details/hotel-details.component';
export { Hotels } from '../hotels.component';
