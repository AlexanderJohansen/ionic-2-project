import {Component, Output, EventEmitter} from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {Transfer, MediaPlugin} from 'ionic-native';

declare var cordova;
declare var Media;


@Component({
  selector: 'voice-button',
  templateUrl: 'build/search/voice-button/voice-button.component.html',
})
export class VoiceButton {
  @Output() onListen = new EventEmitter<any>();

  recording:boolean;
  labelRecord:string;
  src:string;
  mediaRec: any;
  speech: string;
  recordTimeout: any;
 
  constructor(private navCtrl: NavController, public platform: Platform) {   
    this.recording = false;
    this.labelRecord = "Tap to record";
    
    var currLang = window['localStorage']['currentLang'];
  }

  generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
  }

  recordAudio(action) {
    var that = this;
    if(this.recording && action=="stop") {
        clearTimeout(this.recordTimeout);
        this.mediaRec.stopRecord();
        // this.mediaRec.play();
        console.log(this.mediaRec);
        this.labelRecord = "Tap to record";
        this.recording = false;
        this.upload();
        this.speech = "Processing..."
        this.onListen.emit({str: this.speech, complete: false});
        this.mediaRec.release();

    } 
    if(action=="start") {
      if(this.platform.is("android")) {
        this.src = cordova.file.externalApplicationStorageDirectory+"myrecording.amr";
      } else {
        this.src = "myrecording.wav";  
      }
      
      this.mediaRec = new MediaPlugin(this.src);
      this.mediaRec.startRecord();
      this.labelRecord = "Tap to stop";
      this.recording = true;
      this.speech = "Listening..."
      this.onListen.emit({str: this.speech, complete: false});
      this.recordTimeout = setTimeout(function() {
        that.recordAudio("stop");
      }, 9000)
    }
  }
  upload() {
    let ft = new Transfer();
    let fileSrc = this.src;
    let lang = window['localStorage']['currentLang'];
    let filename = lang+"_"+this.generateUUID()+".wav";
    let options = {
        fileKey: 'file',
        fileName: filename,
        mimeType: 'audio/x-wav',
        chunkedMode: false,
        headers: {
            'Content-Type' : undefined
        },
        params: {
            fileName: filename
        }
    }; 

    if(!this.platform.is("android")) {
      fileSrc = cordova.file.tempDirectory+this.src;
    }

    
    ft.upload(fileSrc, "http://162.13.177.130:3399/upload", options)
    .then((result: any) => {
       this.speech = result.response;
       this.onListen.emit({str: this.speech, complete: true});
    }).catch((error: any) => {
        console.log(error)
    }); 
  }
}
