import { Component, Input, forwardRef, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Events, Content,ToastController } from 'ionic-angular';
import * as moment from 'moment';
// import { FileService } from '../../shared/services/file-service.service.ts';
import { TranslateService } from 'ng2-translate/ng2-translate';
import {
  TTSService,
  Favorites,
  AjmanMap,
  Event,
  ImageSlider,
  TopBar,
  ShareButton
} from '../../shared';
declare var cordova;

@Component({
  templateUrl: 'build/+events/event-details/event-details.component.html',
  providers: [Favorites],
  directives: [forwardRef(() => AjmanMap), forwardRef(() => TopBar), forwardRef(() => TTSService), forwardRef(() => ShareButton)],
  styles: [`
  `]
})
export class EventDetails {
  item: Event;
   exitPage: boolean = false;
  enterPage: boolean = true;
  scrollTweak: boolean = false;
  scrollBar: boolean = false;
  imgHeight: any = window['innerHeight'];
  detailsBG: boolean = false;
  @ViewChild('detailScroll') detailScroll: Content;
  didLoad: boolean;
  constructor(public translateService: TranslateService, public toastCtrl: ToastController,public events: Events, public loadingCtrl: LoadingController, public navCtrl: NavController, params: NavParams, public favorites: Favorites) {
    this.item = params.get("item");
    // this.fileService.getFile(this.item.featuredImage, "event")

    var that = this;
    //this.presentLoading();
     events.subscribe("page:exit", function() {
       that.exitPage = true;
    });

  }
    ionViewDidEnter() {
      this.detailsBG = true
    }
 private presentLoading() {
    let loading = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 1000,
      dismissOnPageChange: false
    });
    loading.present();
    loading.onDidDismiss(() => {
      this.didLoad = true;
    });
  }
  
  ngAfterViewInit() {
      this.detailScroll.addScrollListener((event) => {
           var topPos = event.target.querySelector(".details-top").scrollHeight+50;
           this.enterPage = false;
           // if(event.target.scrollTop>topPos) {
           //   console.log("scrollTweak", true);
           //   this.scrollTweak = true;
           // } else {
           //   console.log("scrollTweak", false);
           //   this.scrollTweak = false;
           // }
           if (event.target.scrollTop > 10) {
              if(!this.scrollBar) {
                this.scrollBar = true;
              }
              
            } else {
              if(this.scrollBar) {
                this.scrollBar = false;  
              }
              
            }
      });
  }

  private getItems() {
    return [this.item];
  }
  private formatDate(date: string) {
    return moment(date).format("MMM Do YY");
  }
openURL(day) {
    cordova.InAppBrowser.open('https://www.google.com/maps/place/'+day.latitude+','+ day.longitude+'/@'+day.location+'z', '_system')
  }

toggleFavorite() {
    var str = "";

    if (this.favorites.itemIsFavorite(this.item.id)) {
      str = "Item removed from favorites!";
      this.item.favorite = false;
      this.favorites.removeFromFavorites(this.item.id);
    }
    else {
      str = "Added to favourites!";
      this.item.favorite = true;
      this.favorites.addToFavorites(this.item.id);
    }

    this.translateService.getTranslation(localStorage.getItem("currentLang")).subscribe(res => {
      if (res[str])
        str = res[str];
      var toast = this.toastCtrl.create({
        message: str,
        duration: 3000,
        position: 'bottom'
      })
      toast.present();
    });
    this.events.publish("favourites:change", true);

  }

}
