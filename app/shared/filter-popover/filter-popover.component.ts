import {Component, ChangeDetectorRef} from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import {App} from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { LanguageService } from "../services/language.service";


@Component({
    templateUrl: 'build/shared/filter-popover/filter-popover.component.html',
    providers: [TranslateService, TranslateLoader, LanguageService,{ 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe]
})
export class FilterPopover {
    
    type:string;
    filterForm:any;

    constructor(public translateService:TranslateService,private params: NavParams, private viewCtrl: ViewController, public ref: ChangeDetectorRef) {
        this.type = this.params.get("type");
        this.filterForm = this.params.get("filter");
    }
    ngOnInit() {
        // console.log("filter", this.params.get("filter"))
        // this.filterForm = this.params.get("filter");
        this.translateService.use(window['localStorage']['currentLang']);
    }
    save() {
        this.viewCtrl.dismiss(
            this.filterForm
        );
    }
    close() {
        this.viewCtrl.dismiss();
    }
    changeFilter(evt, type) {
        if(this.type=="itenerary_duration"&&type<5) {
            for(var u = 0; u < 5; u++) {
                this.filterForm[u].checked = false;
            }
        }
        this.filterForm[type].checked = evt.target.checked;
        this.ref.detectChanges();
    }
    isChecked(item) {
        return this.filterForm[item].checked;
    }
}