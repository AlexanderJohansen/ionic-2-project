// import { Component, Input } from '@angular/core';
// import {
//     GOOGLE_MAPS_DIRECTIVES
// } from 'angular2-google-maps/core';
// import {
//     AppSettings
// } from '../';


// @Component({
//     selector: 'ajman-map',
//     templateUrl: 'build/shared/ajman-map/ajman-map.component.html',
//     directives:[GOOGLE_MAPS_DIRECTIVES],
//     styles: [`
//         .sebm-google-map-container {
//             height: 100%;
//             width:100%;
//             position:relative;
//             z-index:1;
//         }
//         .map-overlay {
//             background-color: rgb(0, 0, 0);
//             opacity: 0.2;
//             position: absolute;
//             left: 0px;
//             top: 0px;
//             width: 100%;
//             height: 100%;
//             z-index: 2;
//         }
//     `],
// })
// export class AjmanMap {
//     @Input() items: Array<any>;
//     @Input() showMyLocation: boolean;
//     @Input() centre: any;
//     mapOptions: any;
//     myLocation: any;
//     constructor() {
//         this.mapOptions = {
//             minZoom: 14,
//             maxZoom: 15,
//             draggable: false,
//             zoom: 12,
//             lat: AppSettings.AJMAN_LOCATION_LATITUDE,
//             lng: AppSettings.AJMAN_LOCATION_LONGITUDE,
//             disableDefaultUI: true,
//             zoomZontrol: false,
//             styles:[{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#cde1fe"},{"visibility":"on"}]}]
//         }
//     }

//     ngOnInit(){
//         if(this.showMyLocation)
//         {
//             this.myLocation = {latitude: AppSettings.AJMAN_LOCATION_LATITUDE, longitude: AppSettings.AJMAN_LOCATION_LONGITUDE};
//         }
//     }

// }

import { Component, ElementRef, ViewChild, Input, Output, EventEmitter,OnChanges } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ConnectivityService, AppSettings } from '../';
import { Geolocation } from 'ionic-native';

declare var google;

@Component({
  selector: 'ajman-map',
  templateUrl: 'build/shared/ajman-map/ajman-map.component.html',
  styles: [`
        ajman-map > div {
            height: 100%;
            width:100%;
            position:relative;
            z-index:1;
        }
        .map-overlay {
            background-color: rgb(0, 0, 0);
            opacity: 0.15;
            position: absolute;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 2;
            -webkit-transform: translateZ(0) translate3d(0px, 0px, 0px);
        }
    `],
})
export class AjmanMap {
  @Input() items: Array<any>;
  @Input() showMyLocation: boolean;
  @Input() centre: any;
  @Input() elmID: string = '';
  @Input() center: any;
  @Input() placeIcon: any;
  @Output() dismissLoading = new EventEmitter<boolean>();
  @ViewChild('map') mapElement: ElementRef;

  map: Array<any>;
  mapInitialised: boolean = false;

  elementMap: any;
  mapOptions: any;
  myLocation: any;
  apiKey: any = "AIzaSyA-F3pI7rSUeBNQwDa2RILrNBqW8DAS7PQ";
  markers:Array<any> = [];
  markersObj: any;
  constructor(private nav: NavController, private connectivityService: ConnectivityService) {
    this.map = [];
  }

  ngAfterViewInit() {
      
  }
  ngOnInit() {
    // var mapIndex  = -1;
    //   if(this.elmID) {
    //     mapIndex = this.elmID;
    //   } else {
    //     this.elmID = -1;
    //   }
    // if(this.elmID)
    this.loadGoogleMaps();
  }

  loadGoogleMaps() {

    this.addConnectivityListeners();
    if(window['mapInit'])
    {
      this.initMap();
      console.log('loaded')
      return;
    }
    

    if (typeof google == "undefined" || typeof google.maps == "undefined") {

      console.log("Google maps JavaScript needs to be loaded.");
      this.disableMap();

      if (this.connectivityService.isOnline()) {
        console.log("online, loading map");

        window['mapInit'] = () => {
            this.initMap();
            this.enableMap();
          }

        // let script = document.createElement("script");
        // script.id = "googleMaps";

        // if (this.apiKey) {
        //   script.src = 'http://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit';
        // } else {
        //   script.src = 'http://maps.google.com/maps/api/js?callback=mapInit';
        // }

        //document.body.appendChild(script);
        //var that = this;
        //setTimeout(function() {
          //Load the SDK
          window['mapInit'] = () => {
            this.initMap();
            this.enableMap();
          }
        //}, 200);

      }
    }
    else {

      if (this.connectivityService.isOnline()) {
        console.log("showing map");
        this.initMap();
        this.enableMap();
      }
      else {
        console.log("disabling map");
        this.disableMap();
      }

    }

  }

  initMap() {

      this.mapInitialised = true;

      var latLng = new google.maps.LatLng(
        AppSettings.AJMAN_LOCATION_LATITUDE,
        AppSettings.AJMAN_LOCATION_LONGITUDE
      );
      if(this.center){
        var latLngValues = this.center.split(",");
        latLng = new google.maps.LatLng(
          latLngValues[0],
          latLngValues[1]
        );
      }
    
      this.mapOptions = {
        draggable: true,
        center: latLng,
        zoom: 12,
        disableDefaultUI: true,
        zoomZontrol: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        // styles: [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#e4ecf9" }, { "visibility": "on" }] }]
      }
      // this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

      // var mapsContainer = document.querySelectorAll(".details-map");
      // var mElm: any;
      // if(mapIndex > -1) {
      //   mElm = mapsContainer[mapIndex].querySelector(".mapsDivs");
      // } else {
      //   mElm = document.querySelector(".mapsDivs");
      // }
      var that = this;

      //var mapArr = 

      //setTimeout(function() {
        //var mapElems = document.querySelectorAll(".mapsDivs");
        //for(var i in mapElems) {
          //var mElm = mapElems[i];
          //try {
            //if(mElm && mElm.classList && !mElm.classList.contains("mapRendered")) {
              //mElm.classList.add("mapRendered");
              //var element = document.getElementById(this.mapElement.nativeElement.id);
              //console.log("=======element=======",element,this.elmID)
              setTimeout(function(){
                
                if(this.center){
                  var latLngValues = this.center.split(",");
                  latLng = new google.maps.LatLng(
                    latLngValues[0],
                    latLngValues[1]
                  );
                }
                else
                {
                  latLng = new google.maps.LatLng(
                    AppSettings.AJMAN_LOCATION_LATITUDE,
                    AppSettings.AJMAN_LOCATION_LONGITUDE
                  );
                }
                this.mapOptions = {
                  draggable: true,
                  center: latLng,
                  zoom: 12,
                  disableDefaultUI: true,
                  zoomZontrol: false,
                  mapTypeId: google.maps.MapTypeId.ROADMAP,
                  // styles: [{ "featureType": "administrative", "elementType": "labels.text.fill", "stylers": [{ "color": "#444444" }] }, { "featureType": "landscape", "elementType": "all", "stylers": [{ "color": "#f2f2f2" }] }, { "featureType": "poi", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "road", "elementType": "all", "stylers": [{ "saturation": -100 }, { "lightness": 45 }] }, { "featureType": "road.highway", "elementType": "all", "stylers": [{ "visibility": "simplified" }] }, { "featureType": "road.arterial", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "all", "stylers": [{ "visibility": "off" }] }, { "featureType": "water", "elementType": "all", "stylers": [{ "color": "#e4ecf9" }, { "visibility": "on" }] }]
                }
                that.elementMap = new google.maps.Map(that.mapElement.nativeElement, that.mapOptions);
                console.log(that.elementMap)
                google.maps.event.addListenerOnce(that.elementMap, 'idle', function(){
                    that.drawMyLocation();
                    that.drawCenter();  
                });
                //that.drawMarkers();
                
      
              },200)
              //var mapObj = new google.maps.Map(this.mapElement.nativeElement, that.mapOptions);
              //that.map.push(mapObj);
              
              // if(this.center){
              //   var latLngValues = this.center.split(",");
              //   var centerLatLng = new google.maps.LatLng(
              //     latLngValues[0],
              //     latLngValues[1]
              //   );
              //   console.log("centerLatLng",centerLatLng);
              //   new google.maps.Marker({
              //     map: mapObj,
              //     animation: google.maps.Animation.DROP,
              //     position: centerLatLng
              //   });
              // }

            //}
          // } catch(err) {
          //   console.log(err);
          // }
          
        //}

        //console.log("elemMap", mapIndex);
        that.dismissLoading.emit(true);

      //}, 100)
    
  }
  ngOnChanges(changes) {
     var that = this;
      if(changes["items"] && changes["items"].currentValue != changes["items"].previousValue)
      { 
        setTimeout(function() {
          that.drawMarkers();  
        }, 200)
        
      }

  }


  drawCenter(){

    // console.log("=====center=====",this.center)
    if (this.center) {
      var latLngValues = this.center.split(",");
      var latLng = new google.maps.LatLng(
        latLngValues[0],
        latLngValues[1]
      );
      var marker = new google.maps.Marker({
        visible: true,
        position: latLng,
        map: this.elementMap,
        icon: {
          url: "./imgs/pins/pin-"+this.placeIcon+"-hd.png",
          scaledSize: new google.maps.Size(23, 32)
        }
      });
      
    }

  }

  resetMarkers(){
    if(!this.markers || this.markers.length == 0) return;
    for (var index = 0; index < this.markers.length; index++) {
      var m = this.markers[index];
      m.setMap(null);
    }
    this.markers.length = 0;

  }

  drawMyLocation(){

    if (this.showMyLocation) {
      Geolocation.getCurrentPosition().then((position) => {
        this.myLocation = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        new google.maps.Marker({
          map: this.elementMap,
          animation: google.maps.Animation.DROP,
          position: this.myLocation,
          icon: {
            url: "./imgs/icons/icon-map-user.png",
            scaledSize: new google.maps.Size(60, 60)
          }
        });
      });
      
    }

  }
  drawMarkers() {
    var that = this;
    this.resetMarkers();
    if (!this.items || this.items.length == 0) return;

    this.items.forEach(item => {
        // console.log("map item", item);
        that.markers.push(

          new google.maps.Marker({
            map: that.elementMap,
            position: new google.maps.LatLng(
              item.latitude,
              item.longitude
            ),
            title: item.name,
            icon: {
              url: item.icon,
              scaledSize: new google.maps.Size(23,32)
            }
          })

        );
    });
  }
  disableMap() {
    console.log("disable map");
  }

  enableMap() {
    console.log("enable map");
  }

  addConnectivityListeners() {
    var me = this;
    
    
    var onOnline = () => {
      setTimeout(() => {
        if (typeof google == "undefined" || typeof google.maps == "undefined") {
          this.loadGoogleMaps();
        } else {
          if (!this.mapInitialised) {
            this.initMap();
          }

          this.enableMap();
        }
      }, 2000);
    };

    var onOffline = () => {
      this.disableMap();
    };

    document.addEventListener('online', onOnline, false);
    document.addEventListener('offline', onOffline, false);

  }

}