import { Component, ViewChild, OnInit, ElementRef,
trigger,
state,
style,
transition,
Inject,
animate, ChangeDetectorRef } from '@angular/core';
import { MenuController, Platform, Nav, NavController, ModalController, Events as EventManager } from 'ionic-angular';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
import { Trips } from '../../+trips/trips.component';
import { CameraFrame } from '../../camera/camera.component';

declare var TTS;
declare var CameraPreview: any;

import {
	Hotels,
	Events,
	Attractions,
	Activities,
	Restaurants,
	HomePage,
	FavoriteItems,
	Settings,
	AuthorizationService,
	HttpClient,
	ContentService,
	ImageCache,
	DbService,
	Search,
	SearchResult,
	VirtualTour,
	Favorites,
	TopBar,
	LanguageService,
	TravelInformation,
	AboutAjman,
	VisitorSupport,
	Destinations,
	NearBy,
	CurrencyConverter,
	OffersPromotions,
	HotelDetails
} from '../'


@Component({
  selector: 'ajman-nav',
  templateUrl: 'build/shared/nav/nav.component.html',
  directives: [Settings, SearchResult, TopBar],
  providers: [Nav, ImageCache, ContentService, DbService, HttpClient, AuthorizationService, ModalController, Favorites, NavController, LanguageService],
  pipes: [TranslatePipe],
  //  animations: [
  //   trigger('settingsPage', [
  //     state('close', style({ opacity: "0", top: 'calc(100% - 50px)'})),
  //     state('open',   style({ top: '0'})),
  //     transition('close => *', animate('300ms ease-in')),
  //     transition('* => close', animate('300ms ease-out', style({ opacity: "0" })))
  //   ])
  // ]
})

export class AjmanNav implements OnInit {
	@ViewChild('menuList') private listScroll: ElementRef;
	@ViewChild('content') nav : NavController
	rootPage: any;
	pages: any;
	submenuOpen = 0;
	settingsOpen = false;
	settingsHidden = true;
	settingsFinish = false;
	settingsAnimate = "close"
	isRTL: string;
	favoritesItems: any;
	searchStr: string = "";
	searchItems: Array<any>;
	showTopBar: boolean = true;
	showMenu: boolean = true;
	language: any;
	hideMenu: boolean = false;
	clickExecuted: boolean = true;
	waitingSearch: boolean = false;
	constructor(public dbService: DbService, private languageService: LanguageService,public events: EventManager,public fav: Favorites, private ref: ChangeDetectorRef, public contentService: ContentService, private platform: Platform, private menu: MenuController,  private modalController: ModalController) {
		this.pages = [
		  { id: "01", title: 'Home', component: HomePage, icon: "home" },
		  { id: "01", title: 'Plan Your Trip', component: Trips, icon: "travel" },
		  { id: "02", title: 'Places To Go', component: HomePage, icon: "places", items: [
			{ title: "Top Places", component: Destinations, icon: "most-popular", params: { section: "top" } },
			{ title: "Ajman", component: Destinations, icon: "sub-1", params: { section: "ajman" }  },
			{ title: "Masfout", component: Destinations, icon: "sub-2", params: { section: "masfout" }  },
			{ title: "Al Manama", component: Destinations, icon: "sub-1", params: { section: "manama" }  },
		  ]},
		  { id: "03", title: 'Things To Do', component: Activities, icon: "things"},
		  { id: "04", title: 'Upcoming Events', component: Events, icon: "events" },
		  { id: "05", title: 'Dining Options', component: Restaurants, icon: "dining" },
		  { id: "06", title: 'Find Accommodation', component: Hotels, icon: "accommodation" },
		  { id: "07", title: 'Offers & Promotions', component: OffersPromotions, icon: "offers" },
		  { id: "08", title: 'Capture The Moment', component: CameraFrame, icon: "moment" },
		  { id: "09", title: 'Travel Information', component: TravelInformation, icon: "travel"},
		  { id: "10", title: 'Visitor Support', component: VisitorSupport, icon: "support" },
		  { id: "11", title: 'About Ajman', component: AboutAjman, icon: "about"},
		  
		];
		this.language = languageService;
		if(window['localStorage']['currentLang']=="ar") {
			this.isRTL = "right";
			this.platform.setLang("ar", true);
			this.platform.setDir("rtl", true);
			this.language.setLanguage("ar");
		} else {
			this.isRTL = "left";
			this.platform.setLang("en", true);
			this.platform.setDir("ltr", true);
			this.language.setLanguage("en");
		}

		
		

		this.menu.swipeEnable(false, 'ajmanMenu');
		

		this.favoritesItems = this.fav.getAllFavorites().length;
		var that = this;
		events.subscribe('menu:open', (menuData) => {
		  	that.showMenu = true;
		});

		events.subscribe('language:change', (menuData) => {
		  	this.changelang();
		  	console.log("changeLang")
		});

		events.subscribe('favourites:change', (menuData) => {
		
		  	this.favoritesItems = this.fav.getAllFavorites().length;
		
		});


		events.subscribe('search:open', (menuData) => {
		  	this.speak();
		});

		events.subscribe('camera:open', (menuData) => {
		  	this.hideMenu = true;
		});
		events.subscribe('camera:close', (menuData) => {
		  	this.hideMenu = false;
		});

		this.dbService.destroyDB("init");

		
	}
	ionViewDidEnter() {
		this.platform.ready().then(() => {
			if(this.platform.is("ios")) {
				CameraPreview.stopCamera();
			}	
		})
		
	}

	search(){
		var that = this
		console.log("searchTrigger")
		if(this.searchStr=="") {
			this.searchItems = null;
		} else {
			this.waitingSearch = true;
			this.dbService.search(this.searchStr).subscribe(items=>{
				console.log("serachItems", items)
				that.searchItems = items;
				this.waitingSearch = false;

			});	
		}
		
	}
	searchVal(e) {
		if(e.target.value!=this.searchStr) {
			this.searchStr = e.target.value.trim();
			// this.search();
		}
		
	}

	ngOnInit() {
		console.log("onInit", HomePage);

		this.rootPage = HomePage;
	}

	changelang() {
		if(window['localStorage']['currentLang']=="ar") {
			this.isRTL = "right";
			this.platform.setLang("ar", true);
			this.platform.setDir("rtl", true);
			this.language.setLanguage("ar");
		} else {
			this.isRTL = "left";
			this.platform.setLang("en", true);
			this.platform.setDir("ltr", true);
			this.language.setLanguage("en");
		}
		this.ref.detectChanges();
	}

	showFavorites(){
		var that = this;
		console.log("show favs")
		this.nav.setRoot(FavoriteItems).then((res) => {
			that.menu.close();
			document.getElementById("container-box").classList.remove("menu-content-open");
		})
	}

	showNearby(){
		var that = this;
		this.nav.setRoot(NearBy).then((res) => {
			that.menu.close();
			document.getElementById("container-box").classList.remove("menu-content-open");
		})
	}
	openSearchResult(elm){
		var that = this;
		this.nav.push(elm.page, {item: elm.item, popbutton: true}).then(
			(res) => {
				that.menu.close().then(() => {
					that.closeSearch();
				})
				document.getElementById("container-box").classList.remove("menu-content-open");
			},
			(err) => { console.log(err); }
		);
	}
	openPage(page, e) {
		console.log("executed", this.clickExecuted)

		if(this.clickExecuted) {
			
			var items = document.querySelectorAll("#submenu-"+e+" li").length;
			this.submenuOpen = e;
			
			 {
				this.clickExecuted = false;
				var that = this;
				this.submenuOpen = e;
				this.nav.setRoot(page.component, page.params, {animate: false} ).then((res) => {
					that.menu.close().then(res => {
						that.clickExecuted = true;
						console.log("foiExecuted", this.clickExecuted)
					});
					that.closeSearch();

					document.getElementById("container-box").classList.remove("menu-content-open");
				})
			}
		}
		
 	}
 	openSettings() {
 		this.settingsHidden = false;
 		var that = this;
 		setTimeout(function() {
 			that.settingsOpen = true;
 			that.settingsFinish = true;
 		}, 200)
 		this.settingsAnimate = "open"
 		console.log(this.settingsAnimate)
 		let modal = this.modalController.create(Settings, {active: this.nav.getActive()});
 		modal.present();

 	}
 	closeSettings(e) {
 		var that = this;
 		that.settingsFinish = false;
 		setTimeout(function() {
	 			that.settingsOpen = false;
			setTimeout(function() {
	 			that.settingsHidden = true;
	 		}, 500)
 		}, 500)
 		// this.settingsAnimate = (this.settingsAnimate=="close")?"open":"close";
 		
 	}
 	// speak() {
 	// 	var that = this;
		// 	this.rootPage = Search;
		// 	setTimeout(function() {
		// 		that.menu.close();
		// 		document.getElementById("container-box").classList.remove("menu-content-open");
		// 	}, 400);
 	// }
 	speak() {
 		var that = this;
	    let modal = this.modalController.create(Search);
	    modal.onDidDismiss(data => {
	     that.searchStr =  data.searchStr;
	     that.search();
	     console.log(that.searchStr);
	   });
	    modal.present();
	  }
	  closeSearch() {
	  	this.waitingSearch = false;
	  	this.searchStr = "";
	  	this.search();
	  	// this.menu.close();
	  	// this.clickExecuted = false;
	  }
}