import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/forkJoin';
import * as moment from 'moment';
import { Transfer, File } from 'ionic-native';



import {
  AppSettings,
  HttpClient,
  ImageCache,
  Hotel,
  Trip,
  Attraction,
  Activity,
  Restaurant,
  Event,
  TextPage,
  DistanceCalculator,
  TravelAgent
} from '../';

import '../helpers/extensions';



let PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-upsert'));
PouchDB.plugin(require('pouchdb-find'));
PouchDB.plugin(require('pouchdb-quick-search'));
declare var cordova;

@Injectable()
export class DbService {
  _db: any;
  http: HttpClient;
  moment: any;
  imageFields: Array<string>;
  hotelsIndex: any;
  constructor(private httpClient: HttpClient, public events: Events) {
    this.imageFields = ['logo', 'hotelLogo', 'image', 'featuredImage', 'images', 'bannerImage', 'hotelImages', 'bgImage', 'panoramaImage'];
    this.http = httpClient;
    this.moment = moment;
    window['PouchDB'] = PouchDB;
    this.createDB();
    //this.destroyDB();
    // this.events.subscribe("language:change",this.createDB);
    // this.events.subscribe("app:start", this.initDB);
  }

  createDB() {
    //this.events.unsubscribe("language:change", this.createDB);
    this._db = new PouchDB("AjmanAppDB_" + localStorage.getItem("currentLang"), { adapter: 'websql', iosDatabaseLocation: 'default' });
    

  }
  initDB() {
    ///this.events.unsubscribe("app:start", this.initDB);
    if(window['localStorage']['db_installed']=="true") {
      return;
    }


    var languages = ["en", "ar"];
    var that = this;
    let observableBatch = [];
    languages.forEach(function (lang) {

      var newDB = new PouchDB("AjmanAppDB_" + lang, { adapter: 'websql', iosDatabaseLocation: 'default' });

      that.http.get('json/all_' + lang + ".json")
        .subscribe
        (result => {
          var allItems = new Array<any>();

          result.forEach(element => {
            var excluded_ids = window['localStorage']['excluded_ids'].split(",");
            if(excluded_ids.indexOf(element["_id"]) === -1) { 
              
            // var allValues = element["properties"]["$values"];
            // for (var i = 0; i < allValues.length; i++) {
                
            //     if(that.imageFields.indexOf(allValues[i]["Alias"]) > -1)
            //     {
            //         var queryString = "";
            //         var download_directory = "panorama";
            //         if(allValues[i]["Alias"]!="panoramaImage") {
            //           queryString = "?anchor=center&mode=crop&width=1000&height=500";
            //           download_directory = element.alias;
            //         }
            //         if(allValues[i]["Value"] && allValues[i]["Value"].indexOf(',') > 0)
            //         {
            //           var allImages = allValues[i]["Value"].split(",");
            //           for (var index = 0; index < allImages.length; index++) {
            //             var img = allImages[index];
            //             var url = AppSettings.SERVER_URL + img + queryString;
            //             // console.log(url);
            //             that.downloadFileAsync(url,download_directory).subscribe(function (res) {
            //               console.log(url, res)
            //             }, function (err) {
            //               console.error("checkTheDownloadFile:downloadFileAsync", err);
            //             })
            //           }
            //         }
            //         else
            //         {
            //           var url = AppSettings.SERVER_URL + allValues[i]["Value"] + queryString;
            //           console.log(url);
            //           that.downloadFileAsync(url,download_directory).subscribe(function (res) {
            //             console.log(res)
            //           }, function (err) {
            //             console.error("checkTheDownloadFile:downloadFileAsync", err);
            //           })
            //         }
                    
            //     }

            // }

            switch (element.alias) {
              case "hotel":
                allItems.push(new Hotel(element, null));
                break;
              case "itinerary":
                allItems.push(new Trip(element, null));
                break;
              case "destination":
                allItems.push(new Attraction(element, null));
                break;
              case "event":
                allItems.push(new Event(element, null));
                break;
              case "travelAgent":
                allItems.push(new TravelAgent(element, null));
                break;
              case "activity":
                allItems.push(new Activity(element, null));
                break;
              case "restaurant":
                allItems.push(new Restaurant(element, null));
                break;
              case "TextPage":
                allItems.push(new TextPage(element, null));
                break;
              case "travelAgents":
                if (element.id == 1256) {
                  allItems.push(new TextPage(element, null));
                }
                break;
            }
          }

          });

          

          console.log("allItems!", allItems)
          newDB.bulkDocs({ docs: allItems }, function (err, response) {
            // window['localStorage']['db_installed'] = true;
            console.log("=========================== Initial database for " + lang + " completed ===========================");
          })
        });
    });

  }
 //  downloadFileAsync(url,directory) {
	// 	return Observable.create(observer => {
 //      let storageDirectory: string = cordova.file.cacheDirectory;
	// 		const fileTransfer = new Transfer();
 //      var fileName = url.split('//').pop().split('/').pop();
	// 		var deviceUrl = storageDirectory + directory +'/'+ fileName;
	// 		fileTransfer.download(url, deviceUrl, true).then(res => {
	// 			observer.next(res.nativeURL);
	// 		}).catch(err => {
	// 			console.log("downloadFileAsync:download:error", err, deviceUrl);
				
	// 		});
	// 	})
		
	// }

  //  // var options = {limit : 5};
  //  fetchNextPage(options) {
  //   this._db.allDocs(options, function (err, response) {
  //     if (response && response.rows.length > 0) {
  //       options.startkey = response.rows[response.rows.length - 1];
  //       options.skip = 1;
  //     }
  //     else{
  //       console.log("error")
  //     }
  //     // handle err or response
  //   });
  // }


  // sync(dataUrl) {
  //   console.log(dataUrl)
  //   var that = this
  //   return this.http.get(dataUrl)
  //     .subscribe
  //     (result => {
  //       console.log("Fetching Server: " + this._entity);
  //       var reg = {};
  //       var promises = [];
  //       for (var o in result._embedded.content) {

  //         var item = result._embedded.content[o];
  //         reg[item.id] = item;



  //         reg[item.id]._id = this._entity + "_" + item.id;



  //         delete (reg[item.id]._links);

  //         promises.push(this._db.get(reg[item.id]._id));
  //       }

  //       return Promise.all(promises.map(that.reflect)).then(
  //         (res) => {
  //           var res = res.filter(x => x.status === "resolved");



  //           // -----> Updating existing records
  //           for (var i in res) {
  //             var e = res[i].v;

  //             if (that.moment(e.updateDate).isBefore(reg[e.id].updateDate)) {
  //               that._db.upsert(reg[e.id]._id, function (doc) {
  //                 if (doc.id) {
  //                   var u = doc._id.split("_");
  //                   var i = reg[u[1]];
  //                   delete (reg[u[1]]);
  //                   return i;
  //                 }
  //               })
  //               console.log(["Updating....", e.name]);
  //             } else {
  //               delete (reg[e.id]);
  //             }
  //           }

  //           // ----> Adding new items to DB
  //           for (var i in reg) {
  //             var rs = reg[i];

  //             that._db.putIfNotExists(rs).then(res => {
  //               console.log(["Creating....", res]);
  //             });
  //           }

  //           return that.getAll(this._entity);
  //         }
  //       ).catch(err => { console.log("error", err) });
  //     },
  //     err => {
  //       console.log("Error fetching objects", err);
  //     });
  // }

  // reflect(promise) {
  //   return promise.then(function (v) { return { v: v, status: "resolved" } },
  //     function (e) { return { e: e, status: "rejected" } });
  // }

  // remove(id: any) {
  //   var that = this
  //   this._db.get(this._entity + "_" + id).then(function (doc) {
  //     return that._db.remove(doc);
  //   }, function (e) {
  //     console.log(e)
  //   });
  // }

  destroyDB(w) {
    if(w=="init") {
      var that = this;
      this._db.destroy().then(function () {
        console.log("Database destroyed successfully");
        that.initDB();
      }).catch(function (err) {
        console.error("Error to destroy db", err);
        that.initDB();
      })
    }
    
  }

  search(query: string) {
    this.createDB()
    return Observable.create(observer => {
      console.log("search", query.toLowerCase())
      this._db.search({
        query: query.toLowerCase(),
        fields: ['searchIndex', 'description', 'name', 'cuisine'],
        include_docs: true
      }, function (err, res) {
        console.log("res", res)
        if (err) {
          observer.error(err);
        } else {
          let theData = res.rows.map(row => {
            return row.doc;
          });
          observer.next(theData);
        }
      });
    });
  }
  addDoc(doc) {
    return this._db.putIfNotExists(doc);
  }

  find(type, doc) {
    return this._db.get(doc);
  }

  findById(idSearch) {
    console.log("idSearch", idSearch)
    return Observable.create(observer => {
      this._db.search({
        query: idSearch,
        fields: ['id'],
        include_docs: true
      }, function (err, res) {
        if (err) {
          observer.error(err);
        } else {
          let theData = res.rows.map(row => {
            return row.doc;
          });
          observer.next(theData);
        }
      });
    });
  }
  getIds(ids) {
    return Observable.create(observer => {
      this._db.query(this.mapById, { keys: ids, include_docs: true }).then(doc => {
        if (!doc.rows || doc.rows.length <= 0)
          observer.error("Not Found");
        else
          observer.next(doc);

        observer.complete();

      });
    });
  }
  getMedia(id) {
    return Observable.create(observer => {
      this._db.query(this.mapById, { key: id, include_docs: true }).then(doc => {
        if (!doc.rows || doc.rows.length <= 0)
          observer.error("Not Found");  
        else
          observer.next(doc);

        observer.complete();

      });
    });
  }

  getByIds(ids: Array<number>) {
    var that = this;
    return Observable.create(observer => {
      that._db.createIndex({ index: { fields: ['id'] } }).then(function () {
        that._db.find({ selector: { id: { $in: ids } } }).then(docs => {
          observer.next(docs.docs);
        }).catch(function (err) {
          console.log(err)
        });
      })
    });
  }


  // getByIdsAndAlias(filter: any, fields: Array<string>, skip: number, limit: number) {
  //   var that = this;
  //   return Observable.create(observer => {
  //     that._db.createIndex({ index: { fields: fields } }).then(function () {
  //       that._db.find({
  //         selector: filter,
  //         limit: limit,
  //         skip: skip
  //       }).then(docs => {
  //         observer.next(docs.docs);
  //       }).catch(function (err) {
  //         console.log(err)
  //       });
  //     })
  //   });
  // }
  

 sortByDistanceFunc(coords) {
      return function (a, b) {
        let distanceA = DistanceCalculator.calculateDistance(a, coords);
        let distanceB = DistanceCalculator.calculateDistance(b, coords);
        if (distanceA < distanceB) return -1;
        if (distanceA > distanceB) return 1;
        return 0;
      };
      
  }

   sortBySortOrderFunc()
   {
       return function(res) {
            return res.sort(function (a: any, b: any) {
                if (a.sortOrder < b.sortOrder) return -1;
                if (a.sortOrder > b.sortOrder) return 1;
                return 0;
            });
        }
   }

   sortByDate() {
     return function(res) {
       return res.sort(function(a: any, b: any){
         var dateB = new Date(b.eventStartDate).getTime();
         var dateA = new Date(a.eventStartDate).getTime();
        return dateA - dateB;
      });
     }
     
   }
   sortByIdsSequence(seq, ids) {
     var ordId = [];
     seq.forEach(function(i) {
       for(var t in ids) {
         if(ids[t].id==i) {
           ordId.push(ids[t]);
         }
       }
     });
     return ordId;
   }

  filterItems(items, filter, filterType)
  {
    var filtered = [];
    if(!filter)
      return items;
    if(filterType == 'or'){
       filter.forEach(f => {
        if(f.checked)
        {
          items.forEach(item => {
            if(item[f.field] && item[f.field].indexOf(f.value) > -1) 
              filtered.push(item);
          });
        }
      });
      return filtered.length > 0 ? filtered : items;
    }
    else
    {
      items.forEach(item => {
        var addItem = true;
        filter.forEach(f => {
          if(f.checked){
             console.log(f.field, f.value) 
             if(item[f.field] && item[f.field].indexOf(f.value) == -1)
              addItem = false;
          }
          
        });
        if(addItem){
          console.log(item)
          filtered.push(item);
        }
          
      });
      return filtered;
    }
  } 

  unique(xs) {
    var returnArr = [];
    var ids = [];
    for(var u in xs) {
      var i = xs[u];
      if(ids.indexOf(i.id)<0) {
        returnArr.push(i);
        ids.push(i.id)
      }
    }
    return returnArr;
  }

  getFiltered(selector:any,filter: any, fields: Array<any>, skip: number, limit: number, sortBy:any = 'sortOrder', filterType:string = 'or', coords:any = null) {
    //PouchDB.debug.enable('pouchdb:find') ;

    var that = this;
    that.createDB()
    //console.log("getFiltered:01");
    return Observable.create(observer => {
      // that._db.createIndex({
      //   index: { fields: fields }
      // }).then(function (res) {
        // console.log("getFiltered:02", selector, fields, sortBy);

        that._db.find({
          selector:selector
         })
          .then(docs => {
            //console.log("getFiltered:03");
            var filtered = that.filterItems(docs.docs, filter, filterType);
            var sorted:any;
            //console.log("getFiltered:04", docs);
            var totalItems = docs.docs.length;
            
            if(sortBy == 'sortOrder')
            {

              sorted = that.sortBySortOrderFunc()(filtered);
              

            } else if(sortBy == 'sortIds') {
              var orderIds = selector.id.$in;
              //console.log(orderIds)
              sorted = that.sortByIdsSequence(orderIds, filtered);

            } else if(sortBy=="sortDate") {
              sorted = that.sortByDate()(filtered);
            } else
            {
              sorted = filtered.sort(that.sortByDistanceFunc(coords));
              
            }
            // console.log("sorted", sorted)
            // console.log("sorted", skip,limit)
            // console.log("getFiltered:05", sorted.slice(skip,skip+limit));
            // console.log(sorted)

            console.log("notUnique", sorted);
            sorted = this.unique(sorted);
            console.log("unique", sorted);
            observer.next({"items": sorted.slice(skip,skip+limit), "totalItems": totalItems});
         
          }).catch(function (err) {
            //console.log("getFiltered:error", err)
            observer.next({"items": [], "totalItems": 0});
          });
      });



 

  }

  getAll(alias: string, skip: number, limit: number) {
    //this.destroyDB();
    var that = this;
    console.log("alias:", alias);
    return Observable.create(observer => {

      that._db.createIndex({ index: { fields: ['alias'] } }).then(function () {
        that._db.find({ selector: { 'alias': { $eq: alias } } }).then(
          docs => {
            console.log(docs)
            if (docs.docs && docs.docs.length > 0) {
              observer.next(docs.docs);
            }

          })

          .catch(function (err) {
            console.log(err)
          })

      });


      // this._db.query(that.mapByAlias, { key: alias, include_docs: true })
      //   .then(docs => {

      //     console.log("docs", docs)
      //     if (docs.rows.length > 0) {
      //       let theData = docs.rows.map(row => {
      //         return row.doc;
      //       });
      //       console.log("theData", theData)
      //       observer.next(theData);
      //     }
      //     else {
      //       that.downloadContent(alias).subscribe(docs => {
      //         observer.next(docs);
      //       });
      //     }
      //   })
      // let theData = docs.rows.map(row => {
      //   return row.doc;
      // });

      // // this._db.changes({ live: true, since: 'now', include_docs: true})
      // //     .on('change', this.onDatabaseChange);
      // console.log(theData)
      // observer.next(theData);
      // .catch(function (err) {
      //   console.log(err)
      // })

    });
  }


  private downloadContent(alias) {
    var that = this;
    let observableBatch = [];
    return Observable.create(observer => {
      //console.log("download " + alias + "s");
      var url = AppSettings.API_FULL_URL_BY_NAME(alias);
      that.http.get(url)
        .subscribe
        (result => {
          var returnedData = new Array<any>();
          console.log(result)
          that._db.bulkDocs({ docs: result._embedded.content }, function (err, response) {
            console.log("=========================== all completed ===========================");
            observer.next(result._embedded.content);
          })
          // result._embedded.content.forEach((element) => {
          //   if (element.contentTypeAlias && element.contentTypeAlias === alias) {
          //     observableBatch.push(that.updateItemDetails(element, alias));
          //   }
          // });
          // Observable.forkJoin(observableBatch).subscribe(
          //   result => {
          //     returnedData.push(result);
          //   },
          //   err => console.error(err),
          //   () => {
          //     that._db.bulkDocs({ docs: returnedData[0] }, function (err, response) {
          //       console.log("=========================== all completed ===========================");
          //       observer.next(returnedData[0]);
          //     })
          //   }
          // );
        });
    });
  }

  private updateItemDetails(item, alias) {
    let that = this;
    return Observable.create(innerObserver => {
      var details = item.properties;
      details._id = alias + "_" + item.id;
      details.alias = item.contentTypeAlias;
      details.id = item.id;
      details.name = item.name;
      details.updateDate = item.updateDate;
      that.UpdateItemImagesUrls(details).subscribe(
        x => {
          innerObserver.next(x);

        },
        e => console.log('onError: %s', e),
        () => {
          innerObserver.complete();
        });
    });
  }



  private UpdateItemImagesUrls(item) {
    let that = this;
    let observableBatch = [];
    return Observable.create(innerObserver => {

      for (let p in item) {

        if (that.imageFields.indexOf(p) > -1 && item[p]) {
          observableBatch.push(that.updateFieldUrls(item, p));
        }
      }

      if (observableBatch.length > 0) {
        Observable.forkJoin(observableBatch).subscribe(
          result => {
            innerObserver.next(result[0]);
            innerObserver.complete();
          },
          e => console.log('onError: %s', e)
        );
      }
      else {
        innerObserver.next(item);
        innerObserver.complete();
      }

      //   that.imageFields.forEach((imageField) => {

      //     if (!(item.hasOwnProperty(imageField) && item[imageField])) {
      //       if (i === that.imageFields.length - 1) {
      //         innerObserver.next(item);
      //         innerObserver.complete();
      //         return;
      //       }
      //     }
      //     else {

      //       that.updateFieldUrls(item[imageField]).subscribe(

      //         res => item[imageField] = res

      //       );

      //       var imageIds: Array<string>;
      //       if (item[imageField].indexOf(',') > -1) {
      //         item[imageField] = item[imageField].split(",");
      //         imageIds = item[imageField];
      //       }
      //       else {
      //         imageIds = item[imageField].split(',');
      //       }



      //       imageIds.forEach(id => {
      //         var imageUrl = AppSettings.API_ENDPOINT + AppSettings.API_MEDIA_ROOT + id;
      //         observableBatch.push(this.http.get(imageUrl).map(res => res.json()));
      //       });
      //       Observable.forkJoin(observableBatch).subscribe(
      //         result => {

      //           item[imageField] = result;

      //         }
      //       );


      //       that.http.get(imageUrl)
      //         .subscribe(
      //         res => {
      //           var rs = JSON.parse(res.properties.umbracoFile.replace("src:", '"src":').replace("crops:", '"crops":').replace("'", '"').replace("'", '"'))

      //           if (Array.isArray(item[imageField])) {
      //             var index = item[imageField].indexOf(id);
      //             item[imageField].splice(index, 1, rs.src);
      //           }
      //           else
      //             item[imageField] = rs.src;
      //           i++;
      //           if (ii === aa.length - 1 && iii === aaa.length - 1) {
      //             innerObserver.next(item);
      //             innerObserver.complete();
      //             return;
      //           }
      //         });
      //     });
      // }
      //});
    });
  }
  private updateFieldUrls(item, field) {
    let that = this;
    let observableBatch = [];
    var theField = item[field];
    var theItem = item;
    return Observable.create(innerObserver => {

      var imageIds: Array<string>;
      if (theField.indexOf(',') > -1) {
        theField = theField.split(",");
        imageIds = theField;
      }
      else {
        imageIds = theField.split(',');
      }

      imageIds.forEach(id => {
        var imageUrl = AppSettings.API_ENDPOINT + AppSettings.API_MEDIA_ROOT + id;
        observableBatch.push(
          Observable.create(observer => {
            this.http.get(imageUrl)
              .subscribe(
              res => {
                let rs = JSON.parse(res.properties.umbracoFile.replace("src:", '"src":').replace("crops:", '"crops":').replace("'", '"').replace("'", '"'))
                let src = AppSettings.SERVER_URL + rs.src;
                if (Array.isArray(theField)) {
                  let index = theField.indexOf(id);
                  theField.splice(index, 1, src);
                }
                else
                  theField = src;

                observer.next(theField);
                observer.complete();

              });

          })
        );


      });
      Observable.forkJoin(observableBatch).subscribe(
        f => {
          theItem[field] = f[0];
          innerObserver.next(theItem)
          innerObserver.complete();
        }
      );


    });
  }

  private mapByAlias(doc, emit) {
    emit(doc.alias);
  }

  private mapById(doc, emit) {
    emit(doc.id);
  }

  private mapByName(doc, emit) {
    emit(doc.name);
  }




  // private onDatabaseChange = (change) => {  
  //     var index = this.findIndex(this._data, change.id);
  //     var birthday = this._data[index];

  //     if (change.deleted) {
  //         if (birthday) {
  //             this._data.splice(index, 1); // delete
  //         }
  //     } else {
  //         change.doc.Date = new Date(change.doc.Date);
  //         if (birthday && birthday._id === change.id) {
  //             this._data[index] = change.doc; // update
  //         } else {
  //             this._data.splice(index, 0, change.doc) // insert
  //         }
  //     }
  // }

  // private findIndex(array, id) {  
  //     var low = 0, high = array.length, mid;
  //     while (low < high) {
  //       mid = (low + high) >>> 1;
  //       array[mid]._id < id ? low = mid + 1 : high = mid
  //     }
  //     return low;
  // }

}

