import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { TranslateService } from 'ng2-translate/ng2-translate';
@Injectable()
export class TripAdvisor {
    
    API_KEY = "?key=38ee5e7fc43d42a9839b17d991910bdc&lang=" + localStorage.getItem("currentLang") +"&callback=?";
    URL = "http://api.tripadvisor.com/api/partner/2.0/location/";

    constructor(public http:Http, public translateService:TranslateService){


    } 
    getTripAdvisorDetailsForOneItem(id:string)
    {
        return Observable.create(observer => {
           
            this.http.get(this.URL + id + this.API_KEY)
            .subscribe(res => {

                var r = JSON.stringify(res);
                var resStr = JSON.parse(r);
                var result = JSON.parse(resStr['_body']);
                if(!result) {
                    observer.next("");
                    return;
                }
                var rank = result.ranking_data.ranking_string;
                var rankImage = result.rating_image_url;
                this.translateService.getTranslation(localStorage.getItem("currentLang")).subscribe(res => {
                    var str = res["Tripadvisor traveler Rating"];
                    if(!str)
                        str = "Tripadvisor traveler Rating";

                    var  html = '<p class="tripAdvisorContainer">';
                    html += '<span class="tripadvisor-title">'+str+'</span><br>';
                    html += '<img src="' + rankImage + '">'
                    html+= '<span class="rank-text">' + rank + '</span></p>';
                    
                    observer.next(html);
                })
                
            });
        });
    }

}

