import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Transfer } from 'ionic-native';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/toPromise';

import {
  AppSettings,
  AuthorizationService,
  DbService,
  HttpClient
} from '../';



@Injectable()
export class ImageCache {
  data: any;
  platform: any;
  _db: any;
  _url: string = AppSettings.API_ENDPOINT + AppSettings.API_MEDIA_ROOT;
  _imagesPath: string;
  crop: string = "?anchor=center&mode=crop&width=850&height=640";
  constructor(private http: HttpClient, platform: Platform, dbService: DbService) {
    this.data = null;
    this.platform = platform;
    this._db = dbService;
    this.platform.ready().then(() => {
      this._imagesPath = window['cordova'].file.cacheDirectory + 'www';
    });
  }

  download(imageObject:any) {
    var that = this;
    
    this.platform.ready().then(() => {
      const fileTransfer = new Transfer();
      var rs = JSON.parse(imageObject.properties.umbracoFile.replace("src:", '"src":').replace("crops:", '"crops":').replace("'", '"').replace("'", '"'))
      
      const imageLocation = AppSettings.SERVER_URL + rs.src + that.crop;;

      let targetPath; // storage location depends on device type.

      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is('cordova')) {
        console.log("platform false");
        return false;
      }

      if (this.platform.is('ios')) {
        targetPath = window['cordova'].file.cacheDirectory + "www" + rs.src;
      }
      else if (this.platform.is('android')) {
        targetPath = window['cordova'].file.cacheDirectory + "www" + rs.src;
      }
      else {
        // do nothing, but you could add further types here e.g. windows/blackberry
        return false;
      }
      fileTransfer.download(imageLocation, targetPath).then(
        (result) => {
          that._db.addDoc({id: imageObject.id, _id: "media_" + imageObject.id, alias: "media", "url": targetPath, "updateDate": imageObject.updateDate });
        }
      );
    });
  }

  destroyDB() {
    this._db.destroy();
    var dir = new window['DirectoryEntry']("media", this._imagesPath + "/media");

  }

  getImage(id) {
    var that = this
    return Observable.create(observer => {
      that._db.getMedia("media_" + id).subscribe(
        (res) => {
          observer.next(res.rows[0].doc.url);
        },
        (err) => {
              that.http.get(that._url + id).subscribe
                (result => {
                  var rs = JSON.parse(result.properties.umbracoFile.replace("src:", '"src":').replace("crops:", '"crops":').replace("'", '"').replace("'", '"'))
                  that.data = AppSettings.SERVER_URL + rs.src + that.crop;
                  that.download(result);
                  observer.next(that.data);
                });
        });
    });
  }
  showImage(id) {
    var that = this;
    return this._db.find("media", "media_" + id).then(
      (res) => {
        return res;
      },
      (error) => {
        this.getImage(id);
      }
    )
  }


}

