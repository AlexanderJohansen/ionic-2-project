import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import {
  AppSettings,
  AuthorizationService
} from '../';

@Injectable()
export class HttpClient {
  token: string;
  that:any;
  
  constructor(private http: Http, public auth: AuthorizationService) {
    this.http = http;
    this.that = this;
  }

  // private getAuthorizationHeader() {
  //   var headers = new Headers();
  //   var data = new Observable(observer => {
      
  //     if(!this.tokenExpired())
  //     { 
  //       //observer.next(localStorage.getItem('access_token')); 
  //       observer.complete(localStorage.getItem('access_token'));
  //     }
  //     else
  //     {

  //     }

  //   });
    // if(!this.tokenExpired())
    // {
    //   headers.append('Authorization', 'Bearer ' + localStorage.getItem('access_token')); 
    // }
    // else
    // {
    //     this.requestAccessToken();
    // }
  //   return headers;
  // }

  get(url: string) {
    //console.log(url)
    var headers = new Headers();
    return Observable.create(observer => {
            this.http.get(url, {headers: headers})
            .map(res => res.json())
            .subscribe(res => {
              observer.next(res);
              //observer.complete();
            });
    })
      
  }

  post(url, data) {
    var headers = new Headers();
    this.auth.authorize()
    .subscribe(
      (token: any) => {
        headers.append('Authorization', 'Bearer ' + token); 
        return this.http.post(url, data, {
          headers: headers
        });
      }
    );
  }

  // private requestAccessToken () {
  //   if(!this.tokenExpired())
  //   {
  //     return;
  //   }
  //   var creds = `username=${AppSettings.API_USERNAME}&password=${AppSettings.API_PASSWORD}&grant_type=${AppSettings.API_GRANT_TYPE}`;
  //   var headers = new Headers();
  //   headers.append('Content-Type', 'application/x-www-form-urlencoded');

  //   this.http.post(AppSettings.API_ENDPOINT + 'oauth/token', creds, {
  //     headers: headers
  //     })
  //     .map(res => res.json())
  //     .subscribe(
  //       data => {
  //         console.log(data.access_token);
  //         localStorage.setItem('access_token', data.access_token);
  //         localStorage.setItem('token_expiry', Math.floor(new Date().getTime() / 1000) + data.expires_in);
  //       },
  //       err => console.log(err),
  //       () => console.log('Authentication Complete')
  //     );
  // }



  // private tokenExpired(){
  //   if(!localStorage.getItem('token_expiry') || !localStorage.getItem('access_token'))
  //   {
  //     return true;
  //   }
  //   let expireTime = parseInt(localStorage.getItem('token_expiry'));
  //   let currentTime: number = (Math.floor(new Date().getTime() / 1000));
  //    if(expireTime < currentTime){
  //     return true;
  //   }
  //   return false;  
  // }
}

