import { Component, ViewChild, forwardRef, Input } from '@angular/core';
import { TranslatePipe } from 'ng2-translate/ng2-translate';
import { Events } from 'ionic-angular';
import { NavController} from 'ionic-angular';
import { 
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets
} from '../../shared';

import '../../shared/helpers/extensions';

@Component({
selector: 'top-bar',
  templateUrl: './build/shared/top-bar/top-bar.component.html',
  directives: [ToggleNav,forwardRef(() => HomeWidgets) ],
  providers: [LanguageService],
  pipes: [TranslatePipe]
})
export class TopBar{
	@ViewChild(forwardRef(() => HomeWidgets)) hw: HomeWidgets;
 	@Input() poppage: boolean;
 	@Input() isLoaded: boolean = false;
 	@Input() blackStyle: boolean;
	language: any;
	constructor(language: LanguageService, private navController: NavController, public events: Events) {
		this.language = language;
		
	}
	ngAfterViewInit() {
		// console.log("widgetStart");
		this.hw.startWidgets();
	}
	ngOnDestroy(){
		// console.log("widgetStop");
		this.hw.stopWidgets();
	}
	popNav() {
		if(this.isLoaded) {
			this.navController.pop();
			this.events.publish("page:exit", true);
		}
		

	}
}

