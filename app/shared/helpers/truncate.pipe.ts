import {Pipe} from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe {
  transform(value: string, args: string[]) : string {
    return value.length > 150 ? value.substring(0, 150) + '...' : value;
  }
}