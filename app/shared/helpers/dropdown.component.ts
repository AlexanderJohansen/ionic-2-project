/*
 * Angular 2 Dropdown Multiselect for Bootstrap
 * Current version: 0.1.0
 * 
 * Simon Lindh
 * https://github.com/softsimon/angular-2-dropdown-multiselect
 */
import { Component, Pipe, OnInit, Input, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { Events } from 'ionic-angular';
import { Control } from '@angular/common';
import { Observable } from 'rxjs/Observable';

export interface IMultiSelectOption {
	id: any;
	name: string;
}

export interface IMultiSelectSettings {
	pullRight?: boolean;
	enableSearch?: boolean;
	checkedStyle?: 'checkboxes' | 'glyphicon';
	buttonClasses?: string;
	selectionLimit?: number;
	closeOnSelect?: boolean;
	showCheckAll?: boolean;
	showUncheckAll?: boolean;
	dynamicTitleMaxItems?: number;
    maxHeight?: string;
    isMultiple?: boolean;
}

export interface IMultiSelectTexts {
	checkAll?: string;
	uncheckAll?: string;
	checked?: string;
	checkedPlural?: string;
	searchPlaceholder?: string;
	defaultTitle?: string;
}

@Pipe({
	name: 'searchFilter'
})
class SearchFilter {

	transform(options, args): Array<IMultiSelectOption> {
        
        var opt = options.filter(function(opts) {
            return opts.name.indexOf(args) > -1;
        })
		return opt;
	}
}

@Component({
    selector: 'ss-multiselect-dropdown',
    pipes: [SearchFilter],
	styles: [`
		a { outline: none; }
	`],
    template: `
        <div class="dropdown-group">
            <button type="button" class="dropdown-toggle btn" [ngClass]="settings.buttonClasses" (click)="toggleDropdown()">{{ getTitle() }}&nbsp;<span class="fa fa-caret-down"></span></button>
            <ul *ngIf="isVisible" class="dropdown-menu" [class.pull-right]="settings.pullRight" [style.max-height]="settings.maxHeight" style="display: block; height: auto; overflow-y: auto;">
                <li style="margin: 0px 5px 5px 5px;" *ngIf="settings.enableSearch">
                    <div class="input-group input-group-sm">
                        <input type="text" autocomplete="off"  class="form-control dropdown-search" placeholder="{{ texts.searchPlaceholder }}" aria-describedby="sizing-addon3" [ngFormControl]="search">
                        <span class="input-group-btn" *ngIf="searchFilterText.length > 0">
                            <button class="btn btn-default dropdown-clear" type="button" (click)="clearSearch()"><i class="fa fa-times"></i></button>
                        </span>
                    </div>
                </li>
                
                <li *ngIf="settings.showCheckAll">
                    <a href="#" role="menuitem" tabindex="-1" (click)="checkAll()">
                        <span style="width: 16px;" class="glyphicon glyphicon-ok"></span>
                        {{ texts.checkAll }}
                    </a>
                </li>
                <li *ngIf="settings.showUncheckAll">
                    <a href="#" role="menuitem" tabindex="-1" (click)="uncheckAll()">
                        <span style="width: 16px;" class="glyphicon glyphicon-remove"></span>
                        {{ texts.uncheckAll }}
                    </a>
                </li>
                <li *ngIf="settings.showCheckAll || settings.showUncheckAll" class="divider"></li>
                <li *ngFor="let option of options | searchFilter:searchFilterText" (click)="setSelected($event, option)" [class.dropdown-selected]="isSelected(option)" tappable>
                    <a href="#" role="menuitem" tabindex="-1"  >
                        <input *ngIf="settings.checkedStyle == 'checkboxes'" type="checkbox" [checked]="isSelected(option)" />
                        <span *ngIf="settings.checkedStyle == 'glyphicon'" style="width: 16px;" class="glyphicon" [class.glyphicon-ok]="isSelected(option)"></span>
                        {{ option.name }}
                    </a>
                </li>
            </ul>
        </div>
    `
})
export class MultiselectDropdown implements OnInit {
    @Input() options: Array<IMultiSelectOption>;
    @Input() settings: IMultiSelectSettings;
    @Input() texts: IMultiSelectTexts;
    @Input('defaultModel') selectedModel: Array<number> = [];
    @Input('defaultText') defaultText: string;
    @Output('selectedModel') model = new EventEmitter();
    
    @Output() selectionLimitReached = new EventEmitter();
    @HostListener('document: click', ['$event.target'])
    onClick(target) {
        let parentFound = false;
        while (target !== null && !parentFound) {
            if (target === this.element.nativeElement ) {
                parentFound = true;
            }
            target = target.parentElement;
        }
        if (!parentFound) {
            this.isVisible = false;
        }
    }
    
    private numSelected: number = 0;
    private isVisible: boolean = false;
    private search = new Control();
    private searchFilterText: string = '';
    private defaultSettings: IMultiSelectSettings = {
        pullRight: false,
        enableSearch: false,
        checkedStyle: 'checkboxes',
        buttonClasses: 'btn btn-default',
        selectionLimit: 0,
        closeOnSelect: false,
        showCheckAll: false,
        showUncheckAll: false,
        dynamicTitleMaxItems: 3,
        maxHeight: '300px',
        isMultiple: false
    };
    private defaultTexts: IMultiSelectTexts = {
        checkAll: 'Check all',
        uncheckAll: 'Uncheck all',
        checked: 'checked',
        checkedPlural: 'checked',
        searchPlaceholder: 'Search...',
        defaultTitle: 'Select',
    };
    
    constructor(
        private element: ElementRef,
        public events: Events
    ) { }
    
    ngOnInit() {
        this.settings = Object.assign(this.defaultSettings, this.settings);
        this.texts = Object.assign(this.defaultTexts, this.texts);
        this.updateNumSelected();
        this.search.valueChanges
            .subscribe((text: string) => {
                this.searchFilterText = text;
            });
    }
   
    
    clearSearch() {
        this.search.updateValue('');
    }
    
    toggleDropdown() {
        this.isVisible = !this.isVisible;
    }
    
    modelChanged() {
        this.updateNumSelected();
        this.model.emit(this.selectedModel);
    }
    
    isSelected(option: IMultiSelectOption): boolean {
        return this.selectedModel.indexOf(option.id) > -1;
    }
    
    setSelected(event: Event, option: IMultiSelectOption) {
        var index = this.selectedModel.indexOf(option.id);
        if(this.settings.isMultiple) {
            if (index > -1) {
                this.selectedModel.splice(index, 1);
            } else {
                if (this.settings.selectionLimit === 0 || this.selectedModel.length < this.settings.selectionLimit) {
                    this.selectedModel.push(option.id);
                } else {
                    this.selectionLimitReached.emit(this.selectedModel.length);
                    return;
                }
            }
        } else {
            this.selectedModel = [];
            this.selectedModel.push(option.id);
        }
        if (this.settings.closeOnSelect) {
            this.toggleDropdown();
        }
        this.events.publish("dropdown:change", true);
        this.modelChanged();
    }
    
    getTitle() {
        if (this.numSelected === 0) {
            return this.defaultText;
        }
        if (this.settings.dynamicTitleMaxItems >= this.numSelected) {
            return this.options
                .filter((option: IMultiSelectOption) => this.selectedModel.indexOf(option.id) > -1)
                .map((option: IMultiSelectOption) => option.name)
                .join(', ');
        }

        return this.numSelected + ' ' + (this.numSelected === 1 ? this.texts.checked : this.texts.checkedPlural);
    }
    
    updateNumSelected() {
        this.numSelected = this.selectedModel.length;
    }
    
    checkAll() {
        this.selectedModel = this.options.map(option => option.id);
        this.modelChanged();
    }
    
    uncheckAll() {
        this.selectedModel = [];
        this.modelChanged();
    }
    
}