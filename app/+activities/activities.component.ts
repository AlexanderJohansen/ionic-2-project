import { Component, forwardRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import {
  AuthorizationService,
  DbService,
  HttpClient,
  ImageCache,
  ContentService,
  CardList,
  TopBar,
  ConnectivityService
} from '../shared'

@Component({
  templateUrl: 'build/+activities/activities.component.html',
  providers: [
    DbService,
    AuthorizationService,
    HttpClient,
    ImageCache,
    ContentService,
    ConnectivityService
  ],
  directives: [forwardRef(() => TopBar), forwardRef(() => CardList)] 
})
export class Activities {
  didLoad: boolean;
  loading: any;
  filterValues:any;
  filterFields:Array<any>;
  scrollBar: boolean = false;
  constructor(public loadingCtrl: LoadingController) {
    this.filterValues = {
        beach:false,
        culture:false,
        cycling:false,
        desert:false,
        golf:false,
        scenic:false,
        shopping:false,
        spa:false,
        sports:false,
        watersports:false
    }

    this.filterValues = [
      { field:"category", value: 653, checked: false},
      { field:"category", value: 654, checked: false},
      { field:"category", value: 655, checked: false},
      { field:"category", value: 656, checked: false},
      { field:"category", value: 657, checked: false},
      { field:"category", value: 658, checked: false},
      { field:"category", value: 659, checked: false},
      { field:"category", value: 660, checked: false},
      { field:"category", value: 661, checked: false},
      { field:"category", value: 662, checked: false},
      { field:"category", value: 663, checked: false}
    ];

    //"Nature & Wild Life: 653,Spa: 654,Heritage: 655,Dining: 656,Arts & Culture: 657,Walking & Hiking: 658,Mountain Biking: 659,Water Sports: 660,Golf: 661,Desert Safari: 662,Sightseeing: 663"

    this.filterFields = ["category","sortOrder"];


    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();
  }
  presentLoading() {
    this.loading.present();
  }
  scrolled(value) {
    this.scrollBar = value;
  }
  dismissLoading() {
    //this.loading.dismiss();
  }
}
