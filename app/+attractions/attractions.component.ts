import { Component, forwardRef } from '@angular/core';
import { LoadingController, NavParams } from 'ionic-angular';


import {
  AuthorizationService,
  DbService,
  HttpClient,
  ImageCache,
  ContentService,
  CardList,
  TopBar,
  ConnectivityService
} from '../shared'

@Component({
  templateUrl: 'build/+attractions/attractions.component.html',
  providers: [
    DbService,
    AuthorizationService,
    HttpClient,
    ImageCache,
    ContentService,
    ConnectivityService
  ],
  directives: [forwardRef(() =>TopBar), forwardRef(() => CardList)] 
})
export class Attractions {
  items:any;
  didLoad: boolean;
  loading: any;
  filterValues:any;
  filterFields:Array<any>;
  scrollBar: boolean = false;
  sections: any = {
    "top": [1361, 1367, 1370, 1373],
    "ajman": [1360, 1361, 1362, 1363, 1364, 1365, 1366, 1367, 1368, 1369, 1370, 1372, 1373, 1375, 1376],
    "masfout": [1378, 1379, 1381],
    "manama": [1383, 1385, 1386]
  };
  pages: any = {
    "ajman": [1325],
    "masfout": [1377],
    "manama": [1382]
  }
  section: any;
  pageID: any;
  page:  any;
 

  constructor(public loadingCtrl: LoadingController, private ContentService: ContentService, private params: NavParams) {

    // events.subscribe("detail:close", function(e) {
    //   this.closePage();
    // });
    this.filterValues = [
      { field:"category", value: 331, checked: false},
      { field:"category", value: 332, checked: false},
      { field:"category", value: 333, checked: false},
      { field:"category", value: 334, checked: false}
    ];

    //Beach: 331,Urban: 332,Mountain: 333,Mangroves: 334


    this.filterFields = ["category","sortOrder"];


    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();
    this.section = this.sections[params.get("section")];
    this.pageID = this.pages[params.get("section")];
    
  }
   presentLoading() {
    this.loading.present();
  }
  dismissLoading() {
    //this.loading.dismiss();
  }
  scrolled(value) {
    this.scrollBar = value;
  }
  ngOnInit() {
    var that = this;
    this.ContentService.getByIds(this.pageID).subscribe(
      res => { 
        console.log(res)
        that.page = res;

        that.items = this.section;
        //  that.ContentService.getByIds(this.section).subscribe(
        //   res => { 
            
        //     that.items = res;
        //   },
        //   err => { console.log(err) },
        //   () => { console.log("completed"); this.loading.dismiss(); }
        // );
       }
    );
  }
}
