import { Component, Input, forwardRef, ElementRef, ViewChild, ViewChildren, QueryList, ChangeDetectorRef } from '@angular/core';
import { NavController,NavParams, LoadingController, Events, Content,ToastController } from 'ionic-angular';

// import { FileService } from '../../shared/services/file-service.service.ts';
import { TranslateService } from 'ng2-translate/ng2-translate';
import { InAppBrowser } from 'ionic-native';

declare var google;
declare var cordova;
import {
  FavoriteButton,
  TopBar,
  ContentService,
  HotelRating,
  AjmanMap,
  DbService,
  HttpClient,
  AuthorizationService,
  ImageCache,
  Hotel,
  CardList,
  Favorites,
  ImageSaver,
  ShareButton
} from '../../shared';

@Component({
  templateUrl: 'build/+trips/trip-details/trip-details.component.html',
  providers: [
    ContentService,
    DbService,
    HttpClient,
    AuthorizationService,
    ImageCache,
    Favorites,
    // FileService
  ],
  directives:[forwardRef(() => FavoriteButton),forwardRef(() => ShareButton), forwardRef(() => AjmanMap),  forwardRef(() => TopBar),  forwardRef(() => CardList),  forwardRef(() => ImageSaver)],
  styles:[`
    
    
  `]
})
export class TripDetails {
    @ViewChildren('scrollDay') scrollDays: QueryList<ElementRef>;
    @ViewChild('scrollContent') scrollContent: Content;
    public item:any;
    public accommodations: any;
    public loading: any;
    public didLoad: any;
    exitPage: boolean = false;
    enterPage: boolean = true;
    scrollTweak: boolean = false;
    scrollBar: boolean = false;
    scrollInterval: any;
    public daysScroll: any = -1;
    public disableTopButton = true;
    public disableBottomButton = false;
    bgImage: any = null;
    loadBg: boolean = false;
    imgHeight: any = window['innerHeight'];
    detailsBG: boolean = false;
    recommended: any;
    constructor(public translateService: TranslateService, public toastCtrl: ToastController,  public ref: ChangeDetectorRef, public events: Events, public loadingCtrl: LoadingController, public navCtrl: NavController,  params: NavParams, public ContentService: ContentService,  public _db: DbService, public favorites: Favorites) {
      var that = this;
      this.item = params.get("item");
      
      this.accommodations = {};
      var that = this;
      var dayIndex = 0;
      this.loading = this.loadingCtrl.create({
      content: "Please wait...",      
      duration: 2000,
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();
      this.item['days'].forEach(function(a, index) {
        var accommodationIDS = a.accommodation;
        var dayIndex = index;
        that.accommodations[dayIndex] = accommodationIDS;
        // that.ContentService.getByIds(accommodationIDS).subscribe(
        //   res => { that.accommodations[dayIndex] = res; },
        //   err => console.log(err),
        //   () => console.log("accom", that.accommodations));
        
      });
      this.recommended  = that.accommodations[0];
      events.subscribe("page:exit", function() {
         that.exitPage = true;
      });
      
    }

    ionViewWillLeave() {
      this.scrollContent.addScrollListener((event) => {
        return false;
      });
      
    } 
  ionViewDidEnter() {
    
    console.log("entrou");
    var that = this;
    setTimeout(function() {
      that.detailsBG = true;
      var days = that.scrollDays['_results'];
      that.scrollContent.addScrollListener((event) => {

         var topPos = event.target.querySelector(".details-top").scrollHeight+50;
           that.enterPage = false;
          
           if (event.target.scrollTop > 10) {
              if(!this.scrollBar) {
                this.scrollBar = true;
              }
              
            } else {
              if(this.scrollBar) {
                this.scrollBar = false;  
              }
              
            }

         for(var i in days) {
          var element = days[i].nativeElement;
          var scroller = event.target;
            let index = parseInt(i);
            if(element.offsetTop<(scroller.scrollTop+75) && (element.offsetTop+element.offsetHeight)>(scroller.scrollTop+75)) {
              that.daysScroll = parseInt(i);
            }
            if(index==0&&element.offsetTop>scroller.scrollTop+75) {
              that.daysScroll = -1;
            }
          };
         
          if(that.daysScroll === -1) {
            that.disableTopButton = true;
          } else if (that.daysScroll ===  (days.length-1)) {
            that.disableBottomButton = true;
          } else {
            that.disableTopButton = false;
            that.disableBottomButton = false;
          }

      });
    },300)
      
  }

  presentLoading() {
    this.loading.present();
  }
  dismissLoading() {
    //this.loading.dismiss();
  }


  toggleFavorite() {
    var str = "";

    if (this.favorites.itemIsFavorite(this.item.id)) {
      str = "Item removed from favorites!";
      this.item.favorite = false;
      this.favorites.removeFromFavorites(this.item.id);
    }
    else {
      str = "Added to favourites!";
      this.item.favorite = true;
      this.favorites.addToFavorites(this.item.id);
    }

    this.translateService.getTranslation(localStorage.getItem("currentLang")).subscribe(res => {
      if (res[str])
        str = res[str];
      var toast = this.toastCtrl.create({
        message: str,
        duration: 3000,
        position: 'bottom'
      })
      toast.present();
    });
    this.events.publish("favourites:change", true);

  }

  openURL(day) {
    cordova.InAppBrowser.open('https://www.google.com/maps/place/'+day.latitude+','+ day.longitude+'/@'+day.location+'z', '_system')
  }

  scrollToDay(dir) {
    var curDay = parseInt(this.daysScroll);
    if(curDay<0 && dir=="up") { return; };
    var direction = (dir=="up")?(curDay-1):(curDay+1);
    direction = (direction<0)?0:direction;
    direction = (direction>(this.scrollDays['_results'].length-1))?this.scrollDays['_results'].length-1:direction;
    var that = this;
    var scrollElm = this.scrollContent.getScrollElement();
    var contentElm = this.scrollContent.getNativeElement();
    try {
      var elmTop = this.scrollDays['_results'][direction].nativeElement.offsetTop-70;
      if(dir=="up"&&curDay===0) {
        elmTop = 0;
        this.daysScroll = -1;
      }
      this.scrollContent.scrollTo(0, elmTop+10, 500)
    } catch(err) {
      console.log(err, direction)
    }
    
  }

  getAccommodations(index) {
    var that = this;
    return new Promise(resolve => {
          return that.accommodations[index]
        });
  }
}
