import { Component, forwardRef } from '@angular/core';
import { LoadingController } from 'ionic-angular';

import {
  AuthorizationService,
  DbService,
  HttpClient,
  ImageCache,
  ContentService,
  CardList,
  TopBar,
  ConnectivityService
} from '../shared'

@Component({
  templateUrl: 'build/+nearby/nearby.component.html',
  providers: [
    DbService,
    AuthorizationService,
    HttpClient,
    ImageCache,
    ContentService,
    ConnectivityService
  ],
  directives: [forwardRef(() => TopBar), forwardRef(() => CardList)] 
})
export class NearBy {
  didLoad: boolean;
  loading: any;
  filterValues:any;
  filterFields:Array<any>;
  scrollBar: boolean = false;
  constructor(public loadingCtrl: LoadingController) {
    this.filterValues = [
      { field:"alias", value: 'hotel', checked: false},
      { field:"alias", value: 'restaurant', checked: false},
      { field:"alias", value: 'activity', checked: false},
      { field:"alias", value: 'destination', checked: false},
      { field:"alias", value: 'event', checked: false}
    ];
    
    this.filterFields = ["alias"];


    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
      dismissOnPageChange: false
    });
    this.loading.onDidDismiss(()=>{
      this.didLoad = true;
    });
    //this.presentLoading();
  }
   presentLoading() {
    this.loading.present();
  }
  scrolled(value) {
    this.scrollBar = value;
  }
  dismissLoading() {
    //this.loading.dismiss();
  }
}
