import { Component, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Events as EventManager, ViewController, Nav, NavParams } from 'ionic-angular';
import { InAppBrowser, Toast } from 'ionic-native';
import { Http, Headers } from '@angular/http';
import { TranslatePipe, TranslateService, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { LanguageService } from "../shared/services/language.service";
import {Favorites} from "../shared/";


declare var cordova;

@Component({
  templateUrl: 'build/settings/settings.component.html',
  providers: [Favorites, TranslateService, TranslateLoader, LanguageService,Nav, 
    { 
      provide: TranslateLoader,
      useFactory: (http: Http) => new TranslateStaticLoader(http, 'i18n', '.json'),
      deps: [Http]
    }],
    pipes: [TranslatePipe]
  // host: {
  // 	"class": "settings"
  // }
})

export class Settings {
  @Input() settingsOpen = false;
  @Output() closePage = new EventEmitter();
  language: any;
  languageCheck = false;
  tourCheck = false;
  events: any;

  constructor(public favorites: Favorites,private viewCtrl: ViewController, event: EventManager, public languageService: TranslateService, public ref: ChangeDetectorRef, public nav: Nav, public params: NavParams) {
  	this.language = languageService;
    var currLang = window['localStorage']['currentLang'];
  	var tourOnStart = window['localStorage']['tourOnStart'];
  	if(currLang=="ar") {
  		this.languageCheck = true;
  	}
    if(tourOnStart == "true") {
      this.tourCheck = true;
    }
    this.language.use(window['localStorage']['currentLang']);
    this.events = event;
  }
  resetFavs(){
    this.favorites.resetFavorites();
    var str  = this.languageService.get("Favorites has been reset").subscribe(res => {
      console.log(res)
      Toast.show(res, 'short', 'top').subscribe(
      toast => {
        console.log(toast);
      }
    );
    
    this.events.publish("favourites:change", true);
    })
    
    // Dialogs.alert("Favorites has been reset", "Settings", "Great!");
    
  }

  closeSettings() {
  	// this.closePage.emit(false);
    this.viewCtrl.dismiss();
    
  }

  openLink(url) {
    cordova.InAppBrowser.open(url.replace("[lang]", window['localStorage']['currentLang']), "_system");
  }

  changeTour(e) {
    
    if(e._checked) {
     
      window['localStorage']['tourOnStart'] = true;
    } else {

      window['localStorage']['tourOnStart'] = false;
    } 
  }

  changeLanguage(e) {
  	
  	if(e._checked) {
     
      window['localStorage']['currentLang'] = "ar"
  	} else {

      window['localStorage']['currentLang'] = "en"
  	}
    this.language.use(window['localStorage']['currentLang']);
    console.log(this.params.get("active"));
    this.events.publish("language:change", true);
    
  }

}