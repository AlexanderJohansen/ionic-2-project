import { Component, OnInit, forwardRef } from '@angular/core';
import { LoadingController, NavParams, Events } from 'ionic-angular';
import {
	ContentService,
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar,
  	CardList,
  	DbService,
    HttpClient,
    AuthorizationService,
    ImageCache
} from '../shared'


@Component({
	templateUrl: 'build/travel-agents/travel-agents.component.html',
	providers: [
    ContentService,
    DbService,
    HttpClient,
    AuthorizationService,
    ImageCache,
  ],
  directives: [TopBar, forwardRef(() => CardList)]
})
export class TravelAgents implements OnInit {

	items: any = null;
	loading: any;
	didLoad: any;
	isLoaded: boolean = false;
	pageInfo: any;
	exitPage: boolean = false;
    enterPage: boolean = true;
	 scrollBar: boolean = false;
	 filterFields = ["id","sortOrder"];
	constructor(public loadingCtrl: LoadingController, private ContentService: ContentService, private params: NavParams, public events: Events) {
		this.loading = this.loadingCtrl.create({
	      content: "Please wait...",      
	      duration: 2000,
	      dismissOnPageChange: false
	    });
	    this.loading.onDidDismiss(()=>{
	      this.didLoad = true;
	    });
	    //this.presentLoading();
	    this.pageInfo = params.get('item');
	    var that = this;
	     events.subscribe("page:exit", function() {
	         that.exitPage = true;
	      });
	}

	ngOnInit() {
		var that = this;
		var listArr = [];
		console.log(this.pageInfo);
		this.pageInfo.children.forEach(function(e) {
			var listItem = [];
			console.log("TravelAgent", e)
			listItem['name'] = e.name;
			listItem['alias'] = 'travelAgent';
			listItem['id'] = e.id;
			listItem['email'] = e.properties['$values'][0]['Value'];
			listItem['phone'] = e.properties['$values'][2]['Value'];
			listItem['website'] = e.properties['$values'][3]['Value'];
			listItem['description'] = e.properties['$values'][8]['Value'];
			// listArr.push(listItem);
			listArr.push(e.id);
		})
		console.log(listArr)
		this.items = listArr;
	}
	ionViewDidEnter() {
		this.isLoaded = true;
	}
	presentLoading() {
      this.loading.present();
    }
    dismissLoading() {
      //this.loading.dismiss();
    }
      scrolled(value) {
	    this.scrollBar = value;
	  }
}