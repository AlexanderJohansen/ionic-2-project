import { Component, OnInit, forwardRef } from '@angular/core';
import { LoadingController, NavParams } from 'ionic-angular';
import {
	ContentService,
	ToggleNav,
	LanguageService,
	MultiselectDropdown, 
	IMultiSelectOption,
	HomeWidgets,
	Hotels,
	Attractions,
	Activities,
	Restaurants,
	Intro,
	TopBar,
  	CardList,
  	DbService,
    HttpClient,
    AuthorizationService,
    ImageCache
} from '../shared'


@Component({
	templateUrl: 'build/+destinations/destinations.component.html',
	providers: [
    ContentService,
    DbService,
    HttpClient,
    AuthorizationService,
    ImageCache,
  ],
  directives: [TopBar, forwardRef(() => CardList)]
})
export class Destinations implements OnInit {

	items: any = null;
	page: any = null;
	loading: any;
	didLoad: any;
	pages: any = {
	    "ajman": localStorage.getItem("currentLang") == "en" ? [1325] : [3116],
	    "masfout": localStorage.getItem("currentLang") == "en" ? [1377] : [3133],
	    "manama": localStorage.getItem("currentLang") == "en" ? [1382] : [3138]
  	}
  	destination:  any;
  	icon: string;
  	isTop: boolean = false;
  	scrollBar: boolean = false;
	constructor(public loadingCtrl: LoadingController, private dbService: DbService, public params: NavParams) {
		this.loading = this.loadingCtrl.create({
	      content: "Please wait...",      
	      duration: 2000,
	      dismissOnPageChange: false
	    });
	    this.loading.onDidDismiss(()=>{
	      this.didLoad = true;
	    });
	    //this.presentLoading();
	    this.icon = params.get("section");
	    this.destination = this.pages[params.get("section")];
	}

	ngOnInit() {
		var that = this;
		console.log("DestinationPage", this.destination)
		if(this.icon=="top") {
			this.dbService.getFiltered({featured: 1}, [], ["featured"], 0, 30).subscribe(res => {
				console.log(res);
		    	// that.page = res.items[0];
		    	that.icon = "most-popular";
		    	that.isTop = true;
		    	var page = [];
		    	page['name'] = "Top Places";
		    	page['featuredImage'] = "";
		    	page['subtitle'] = "Ajman is a laid-back, sun-drenched destination in the United Arab Emirates with white-sand beaches, rolling desert dunes, craggy mountain backdrops and thriving mangrove forests.";
		    	that.page = page;
		    	console.log("thepgae", that.page);
		    	var featuredItems = res.items.map((item) => {
		    		return item.id;
		    	})
		    	console.log(featuredItems);
		    	that.items = featuredItems;
		    })
		} else {
			this.dbService.getFiltered({id: {$eq : this.destination[0]}}, [], ["id"], 0, 1).subscribe(res => {
				console.log(res);
		    	that.page = res.items[0];
		    	console.log("thepgae", that.page);
		    	that.items = that.page['children'];
		    })	
		}
		
	}
	presentLoading() {
      this.loading.present();
    }
    dismissLoading() {
      //this.loading.dismiss();
    }
    scrolled(value) {
	    this.scrollBar = value;
	  }
}